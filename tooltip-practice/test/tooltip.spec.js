
var tooltip = require('../src/js/tooltip.js');

describe('tooltop', function() {
    beforeEach(function() {
        setUpHTMLFixture();
        jasmine.clock().install();
    });

    afterEach(function() {
        jasmine.clock().uninstall();
        tooltip.remove("#first");
    });

    describe('add', function() {
        it('Instance creation when tooltips are added.', function() {
            // 툴팁 추가
            tooltip.add('#first');
            expect(tooltip.elementInstances['#first']).not.toBeUndefined();
        });

        it('Check the contents when adding a tooltip.', function() {
            tooltip.add('#first', {
                contents: 'test message',
                delay: 300
            });
            expect(tooltip.elementInstances['#first'].contents).toEqual('test message');
        });
        it('Mouse enter action.', function() {
            tooltip.add('#first', {
                contents: 'test message',
                delay: 300
            });

            trigger('mouseenter', document.getElementById('first'));
            jasmine.clock().tick(200);
            expect($('#first .tooltip').hasClass('show')).toBe(false);
            jasmine.clock().tick(300);
            expect($('#first .tooltip').hasClass('show')).toBe(true);
        });
        it('Mouse leave action.', function() {
            tooltip.add('#first', {
                contents: 'test message',
                delay: 300
            });
            trigger('mouseenter', document.getElementById('first'));
            jasmine.clock().tick(300);
            expect($('#first .tooltip').hasClass('show')).toBe(true);
            trigger('mouseleave', document.getElementById('first'));
            jasmine.clock().tick(100);
            expect($('#first .tooltip').hasClass('show')).toBe(false);
        });
    });

    it('The event should not work when deleted.', function() {
        tooltip.add('#first', {
            contents: 'test message',
            delay: 300
        });
        trigger('mouseenter', document.getElementById('first'));
        jasmine.clock().tick(300);
        expect($('#first .tooltip').html()).toEqual('test message');

        trigger('mouseleave', document.getElementById('first'));
        jasmine.clock().tick(300);

        tooltip.remove('#first');
        //expect($('#first .tooltip').length === 0).toBe(true);
        //expect(tooltip.elementInstances['#first']).toBeUndefined();
    });

    it('It should work with new contents when it is modified.', function() {
        tooltip.add('#first', {
            contents: 'pre message',
            delay: 300
        });

        jasmine.clock().tick(300);
        tooltip.edit('#first', {
            contents: 'Lorem ipsum',
            delay: 100
        });

        trigger('mouseenter', document.getElementById('first'));
        jasmine.clock().tick(100);

        expect($('#first .tooltip').html()).toEqual('Lorem ipsum');
        expect($('#first .tooltip').hasClass('show')).toBe(true);
    });

    it('i', function() {
        tooltip.add('#first', {
            contents: 'pre message',
            delay: 300
        });
        jasmine.clock().tick(300);

        tooltip.init([
            {
                element: '#first',
                contents: 'Duis aute irure dolor',
                delay: 500
            },
            {
                element: '.second',
                contents: 'labore et dolore magna aliqua'
            }
        ]);

        trigger('mouseenter', document.querySelector('.second'));
        jasmine.clock().tick(1);
        expect($('.second .tooltip').html()).toEqual('labore et dolore magna aliqua');

        trigger('mouseenter', document.querySelector('#first'));
        jasmine.clock().tick(500);
        expect($('#first .tooltip').html()).toEqual('Duis aute irure dolor');

        tooltip.remove('.second');
    });
});

/**
 * event trigger
 * @param {string} eventName - eventName
 * @param {HTMLElement} elementSeletor - event target domelement
 */
function trigger(eventName, elementSeletor) {
    var evt = document.createEvent('HTMLEvents');
    var el = elementSeletor;
    evt.initEvent(eventName, true, true);
    el.dispatchEvent(evt);
}

/**
 * make default test html
 */
function setUpHTMLFixture() {
    var stringBuffer = ['<p>The standard Lorem Ipsum passage, used since the 1500s</p>', '<p>', '"Lorem ipsum dolor sit amet, <strong id="first">consectetur</strong> adipiscing elit,', 'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute', 'irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat', '<strong class="second">nulla pariatur</strong>. Excepteur sint occaecat cupidatat non proident,', 'sunt in culpa qui officia deserunt mollit anim id est laborum."', '</p><input type="text" id="clipboardtemp" value="333" />'];

    $('body').html('');
    $('body').append(stringBuffer.join(''));
}
