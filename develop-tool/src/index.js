'use strict';

var hello = require('./module-a');
var world = require('./module-b');

module.exports = {
    hello: hello,
    world: world
};
